FROM gcr.io/spark-operator/spark:v3.1.1

WORKDIR /app
ADD tea3-assembly-1.0.jar tea3.jar

ENTRYPOINT ["/opt/entrypoint.sh"]