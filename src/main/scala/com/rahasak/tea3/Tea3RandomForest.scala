package com.rahasak.tea3

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{MinMaxScaler, OneHotEncoder, StringIndexer, VectorAssembler}
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.ml.tuning.{CrossValidator, CrossValidatorModel, ParamGridBuilder}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Tea3RandomForest extends App {

  // context for spark
  private val spark = SparkSession.builder
    //.master("local[*]")
    .appName("lambda")
    .config("spark.shuffle.file.buffer", "1m") // Set the shuffle file buffer size
    .getOrCreate()

  //  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)
  //  spark.conf.set("spark.sql.broadcastTimeout", 600)
  //  println(spark.conf.get("spark.sql.autoBroadcastJoinThreshold"))
  //  println(spark.conf.get("spark.sql.broadcastTimeout"))

  // configuration for minio s3 storage api
  // fs.s3a.access.key is the MINIO_ROOT_USER
  // fs.s3a.secret.key is the MINIO_ROOT_PASSWORD
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.access.key", "rahasak_minio_user")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.secret.key", "rahasak_minio_password")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.endpoint", "http://192.168.1.36.:9000")

  import spark.implicits._

  // read csv data to data frame
  private val dataPath = "s3a://rahasak/tea3.csv"
  private val tea3Df = spark.read.format("csv")
    .option("header", value = true)
    .option("delimiter", ",")
    .option("mode", "DROPMALFORMED")
    .option("inferSchema", value = true)
    .load(dataPath)
    //.load(getClass.getResource("/tea3.csv").getPath)
    .drop("subElevation")
    .drop("c21")
    .drop("c22")
    .drop("year")
    .drop("sellingEndTime")
    .cache()
  tea3Df.show()

  // find empty data rates
  private val missingRowCount1 = tea3Df.filter(col("st_02").isNull || col("st_02").isNaN).count()
  private val missingRowCount2 = tea3Df.filter(col("st_03").isNull || col("st_03").isNaN).count()
  private val missingRowCount3 = tea3Df.filter(col("ad_02").isNull || col("ad_02").isNaN).count()
  private val missingRowCount4 = tea3Df.filter(col("ad_03").isNull || col("ad_03").isNaN).count()
  println(s"missing st_02 $missingRowCount1")
  println(s"missing st_03 $missingRowCount2")
  println(s"missing ad_02 $missingRowCount3")
  println(s"missing ad_03 $missingRowCount4")

  // prefill empty data
  private val tea3DfPreFilled = tea3Df
    .filter($"status" === "Sold")
    .na.fill("Normal_01", Seq("st_01"))
    .na.fill("Normal_02", Seq("st_02"))
    .na.fill("Normal_03", Seq("st_03"))
    .na.fill("Normal_01", Seq("ad_01"))
    .na.fill("Normal_02", Seq("ad_02"))
    .na.fill("Normal_03", Seq("ad_03"))
    .na.fill("NormalR", Seq("rmk"))
    .na.fill("NormalLR", Seq("lrmk"))
  //.na.drop()
  tea3DfPreFilled.show(20)

  // cast data types
  private val tea3DfCast = tea3DfPreFilled
    .withColumn("weight", col("weight").cast("double"))
    .withColumn("date", col("date").cast("int"))
    .withColumn("week", col("week").cast("int"))
    .withColumn("month", col("month").cast("int"))
    .withColumn("price", col("price").cast("double"))
    .withColumn("id", monotonically_increasing_id())

  // find frequencies of st_01
  private val st1MeanPurchasedPrice = tea3DfCast.groupBy("st_01")
    .agg(mean("price").alias("st_01_mean_purchased_price"))
  st1MeanPurchasedPrice.show(20)
  private val st1Freq = tea3DfCast.groupBy("st_01").count()
  st1Freq.show(20)
  private val st1JoinedData = st1Freq.join(st1MeanPurchasedPrice, Seq("st_01"))
  st1JoinedData.show(20)
  private val st1EncodedData = tea3DfCast.join(st1JoinedData, Seq("st_01"), "left")
  st1EncodedData.show()

  // find frequencies of st_02
  private val st2MeanPurchasedPrice = tea3DfCast.groupBy("st_02")
    .agg(mean("price").alias("st_02_mean_purchased_price"))
  st2MeanPurchasedPrice.show(20)
  private val st2Freq = tea3DfCast.groupBy("st_02").count()
  st2Freq.show(20)
  private val st2JoinedData = st2Freq.join(st2MeanPurchasedPrice, Seq("st_02"))
  st2JoinedData.show(20)
  private val st2EncodedData = st1EncodedData.join(st2JoinedData, Seq("st_02"), "left")
  st2EncodedData.show()

  // find frequency of ad_01
  private val ad1MeanPurchasedPrice = tea3DfCast.groupBy("ad_01")
    .agg(mean("price").alias("ad_01_mean_purchased_price"))
  ad1MeanPurchasedPrice.show(20)
  private val ad1Freq = tea3DfCast.groupBy("ad_01").count()
  ad1Freq.show(20)
  private val ad1JoinedData = ad1Freq.join(ad1MeanPurchasedPrice, Seq("ad_01"))
  ad1JoinedData.show(20)
  private val ad1EncodedData = st2EncodedData.join(ad1JoinedData, Seq("ad_01"), "left")
  ad1EncodedData.show()

  // find frequency of ad_02
  private val ad2MeanPurchasedPrice = tea3DfCast.groupBy("ad_02")
    .agg(mean("price").alias("ad_02_mean_purchased_price"))
  ad2MeanPurchasedPrice.show(20)
  private val ad2Freq = tea3DfCast.groupBy("ad_02").count()
  ad2Freq.show(20)
  private val ad2JoinedData = ad2Freq.join(ad2MeanPurchasedPrice, Seq("ad_02"))
  ad2JoinedData.show(20)
  private val ad2EncodedData = ad1EncodedData.join(ad2JoinedData, Seq("ad_02"), "left")
  ad2EncodedData.show()

  // price to label
  private val labelDf = ad2EncodedData.withColumnRenamed("price", "label")
    .orderBy("id")
    .na.drop()
  labelDf.printSchema()
  labelDf.show(20)

  // indexers to convert grade, category, rp, rmk, lrmk, outstype to integers
  private val gradeIndexer = new StringIndexer().setInputCol("grade").setOutputCol("gradeIndexer").setHandleInvalid("skip")
  private val categoryIndexer = new StringIndexer().setInputCol("category").setOutputCol("categoryIndexer").setHandleInvalid("skip")
  private val rpIndexer = new StringIndexer().setInputCol("rp").setOutputCol("rpIndexer").setHandleInvalid("skip")
  private val rmkIndexer = new StringIndexer().setInputCol("rmk").setOutputCol("rmkIndexer").setHandleInvalid("skip")
  private val lrmkIndexer = new StringIndexer().setInputCol("lrmk").setOutputCol("lrmkIndexer").setHandleInvalid("skip")
  private val outstypIndexer = new StringIndexer().setInputCol("outstype").setOutputCol("outstypeIndexer").setHandleInvalid("skip")

  // encoder
  private val inCols = Array("gradeIndexer", "categoryIndexer", "rpIndexer", "rmkIndexer", "lrmkIndexer", "outstypeIndexer")
  private val outCols = Array("gradeIndexerEnc", "categoryIndexerEnc", "rpIndexerEnc", "rmkIndexerEnc", "lrmkIndexerEnc", "outstypeIndexerEnc")
  private val encoder = new OneHotEncoder()
    .setInputCols(inCols)
    .setOutputCols(outCols)

  // vector assembler for numerical features
  private val numericalCols = Array("weight", "st_01_mean_purchased_price", "st_02_mean_purchased_price", "ad_01_mean_purchased_price", "ad_02_mean_purchased_price", "date", "week", "month")
  private val assembler = new VectorAssembler()
    .setInputCols(numericalCols)
    .setOutputCol("numerical_features")
  private val scaler = new MinMaxScaler().setInputCol("numerical_features").setOutputCol("scaled_features")

  // vector assembler with feature column
  private val featureCols = outCols ++ Array("scaled_features")
  private val featureAssembler = new VectorAssembler()
    .setInputCols(featureCols)
    .setOutputCol("features")

  // build model
  private val model = new RandomForestRegressor()
    .setLabelCol("label")
    .setFeaturesCol("features")
  private val stages = Array(gradeIndexer, categoryIndexer, rpIndexer, rmkIndexer, lrmkIndexer, outstypIndexer, encoder, assembler, scaler, featureAssembler, model)
  private val pipeline = new Pipeline().setStages(stages)
  private val NumTrees = Seq(5, 10, 15)
  private val MaxBins = Seq(23, 27, 30)
  private val numFolds = 10
  private val MaxDepth: Seq[Int] = Seq(20)
  private val paramGrid = new ParamGridBuilder()
    .addGrid(model.numTrees, NumTrees)
    .addGrid(model.maxDepth, MaxDepth)
    .addGrid(model.maxBins, MaxBins)
    .build()

  // split data set training(70%) and test(30%)
  private val seed = 5043
  private val Array(trainingData, testData) = labelDf.randomSplit(Array(0.7, 0.3), seed)

  // cross validate model
  private val cv = new CrossValidator()
    .setEstimator(pipeline)
    .setEvaluator(new RegressionEvaluator)
    .setEstimatorParamMaps(paramGrid)
    .setNumFolds(numFolds)
  private val cvModel = cv.fit(trainingData)

  // prediction with test data
  private val predictions = cvModel.transform(testData)
  predictions.printSchema()
  predictions.show(10)
  predictions.select("label", "prediction").show(200)

  // prediction error percentage
  private val errorData = predictions.withColumn("errorPercentage", (col("prediction") - col("label")) / col("label") * 100)
  errorData.show(200)

  // evaluate model
  // root mean squared error
  private val eval1 = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("rmse")
  private val rmse = eval1.evaluate(predictions)
  println("root mean squared error (RMSE) on test data = " + rmse)

  // r-squared
  private val eval2 = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("r2")
  private val r2 = eval2.evaluate(predictions)
  println("r-squared (r^2) on test data = " + r2)

  private val accuracyPercentage = r2 * 100
  println(s"model accuracy = $accuracyPercentage%")

  // mean absolute error
  private val eval3 = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("mae")
  private val mae = eval3.evaluate(predictions)
  println("mean absolute error (MAE) on test data = " + mae)

  // mean squared error
  private val eval4 = new RegressionEvaluator()
    .setLabelCol("label")
    .setPredictionCol("prediction")
    .setMetricName("mse")
  private val mse = eval4.evaluate(predictions)
  println("mean squared error (MSE) on test data = " + mse)

  // save model in minio s3 bucket
  private val modelPath = "s3a://rahasak/tea3-model"
  cvModel.save(modelPath)

  // load saved model for test
  val cvModelLoaded = CrossValidatorModel.load(modelPath)
}
