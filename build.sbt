name := "tea3"

version := "1.0"

scalaVersion := "2.12.10"

libraryDependencies ++= {
  Seq(
    "org.apache.spark"                %% "spark-core"                     % "3.0.0",
    "org.apache.spark"                %% "spark-sql"                      % "3.0.0",
    "org.apache.spark"                %% "spark-mllib"                    % "3.0.0",
    "org.slf4j"                       % "slf4j-api"                       % "1.7.5",
    "ch.qos.logback"                  % "logback-classic"                 % "1.0.9",
    "org.apache.hadoop"               % "hadoop-client"                   % "2.7.4",
    "org.apache.hadoop"               % "hadoop-aws"                      % "2.7.4",
    "com.fasterxml.jackson.module"    %% "jackson-module-scala"           % "2.10.0",
  )
}

excludeDependencies ++= Seq(
  ExclusionRule("org.apache.avro","avro-tools")
)

assemblyMergeStrategy in assembly := {
  case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case "application.conf" => MergeStrategy.concat
  case x => MergeStrategy.first
}
